import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;


public class Main extends Application {

    private static final int KACHEL_GROESSE = 50;
    private static final int W = 600;
    private static final int H = 400;

    private static final int X_Kacheln = W / KACHEL_GROESSE;
    private static final int Y_Kacheln = H / KACHEL_GROESSE;

    private Kachel[][] spielfeld = new Kachel[X_Kacheln][Y_Kacheln];
    private Scene scene;


    @Override
    public void start(Stage primaryStage) {
        scene = new Scene(starteSpiel());

        primaryStage.setScene(scene);
        primaryStage.setTitle("Minessweeper");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }




    private Parent starteSpiel() {
        Pane root = new Pane();
        root.setPrefSize(W, H);

        for (int y = 0; y < Y_Kacheln; y++) {
            for (int x = 0; x < X_Kacheln; x++) {
                Kachel kachel = new Kachel(x, y, Math.random() < 0.25); //berechnung der bombenanzahl

                spielfeld[x][y] = kachel;
                root.getChildren().add(kachel);
            }
        }

        for (int y = 0; y < Y_Kacheln; y++) {
            for (int x = 0; x < X_Kacheln; x++) {
                Kachel kachel = spielfeld[x][y];

                if (kachel.hatBombe)
                    continue;

                long bomben = getNachbar(kachel).stream().filter(e -> e.hatBombe).count();

                if (bomben > 0)
                    kachel.text.setText(String.valueOf(bomben));
            }
        }

        return root;
    }

    private List<Kachel> getNachbar(Kachel kachel) {
        List<Kachel> nachbar = new ArrayList<>();


        int[] punkte = new int[]{   //ooo
                -1, -1,             //oXo
                -1, 0,              //ooo
                -1, 1,
                0, -1,
                0, 1,
                1, -1,
                1, 0,
                1, 1
        };

        for (int i = 0; i < punkte.length; i++) {
            int dx = punkte[i];
            int dy = punkte[++i];

            int newX = kachel.x + dx;
            int newY = kachel.y + dy;

            if (newX >= 0 && newX < X_Kacheln
                    && newY >= 0 && newY < Y_Kacheln) {
                nachbar.add(spielfeld[newX][newY]);
            }
        }

        return nachbar;
    }

    private class Kachel extends StackPane {
        private int x, y;
        private boolean hatBombe;
        private boolean isAufgedeckt = false;

        private Rectangle rand = new Rectangle(KACHEL_GROESSE - 2, KACHEL_GROESSE - 2);  // groessen -2 da sonst augenkrebs
        private Text text = new Text();

        public Kachel(int x, int y, boolean hatBombe) {
            this.x = x;
            this.y = y;
            this.hatBombe = hatBombe;

            rand.setStroke(Color.BLUE);

            text.setFont(Font.font(18));
            text.setText(hatBombe ? "X" : "");
            text.setVisible(false);

            getChildren().addAll(rand, text);

            setTranslateX(x * KACHEL_GROESSE);
            setTranslateY(y * KACHEL_GROESSE);

            setOnMouseClicked(e -> aufgedeckt());
        }

        public void aufgedeckt() {
            if (isAufgedeckt)
                return;

            if (hatBombe) {

                new Alert(Alert.AlertType.INFORMATION, "Bombe getrofffen, Spiel vorbei!").showAndWait();

                scene.setRoot(starteSpiel());
                return;
            }

            isAufgedeckt = true;
            text.setVisible(true);
            rand.setFill(null);

            if (text.getText().isEmpty()) {
                getNachbar(this).forEach(Kachel::aufgedeckt);
            }
        }
    }


}

